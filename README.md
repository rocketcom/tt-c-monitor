# TT&C Sample Apps: Monitor

## Description
For operators of a TT&C service, maintaining situational awareness is of critical importance, and the TT&C Monitor app is designed to support this requirement. During the UX research effort, operators expressed a desire for a quick and efficient way to view overall status of their constellation and all of their systems, something lacking in their current systems. To deliver on this, the design team worked with domain experts and the operators to identify the most important data and then display it in a clear, logical manner in the app.  

As operators’ primary TT&C app, the Monitor app would constantly occupy one of their large displays.  The main usage would be in between contacts, when operators would use it to keep an eye on system and constellation health, prepare for upcoming contacts and view system trends.

## Prerequisites
1. A UNIX-like OS (Linux, Mac OS, FreeBSD, etc.)*
2. [Node.js](https://nodejs.org/)
3. [npm](https://www.npmjs.com/get-npm)
4. [Polymer](https://www.polymer-project.org/)
5. [EGS Web Socket Server](https://bitbucket.org/rocketcom/egs-socket-server/src/master/) (optional)**
6. [EGS Web Services Server](https://bitbucket.org/rocketcom/egs-data-services/src/master/) (optional)**

\* It may be possible to run on Windows but has not been thoroughly tested and is not recommended.
\*\* You only need to run your own Web Socket and Web Services servers if you are behind a firewall that prevents you from accessing the ones we provide at <wss://sockets.astrouxds.com> and <https://services.astrouxds.com> respectively.


## Getting Started
### Clone this repository

`git clone git@bitbucket.org:rocketcom/tt-c-monitor.git`

### Install the Polymer CLI

`npm i -g polymer-cli`

### Install NPM modules for this project

`npm i`

### Create a symbolic link to the appropriate file in the `config` directory.

We recommend linking to config.local.json:

`ln -s config.local.json config.json`

However, if you are running your own Web Socket and Web Services servers (see above) you will need to create your own config file that points to your own servers. Use one of the existing config files as a template and substitute the appropriate URLs.

### For local development and testing

`npm run start`

### To build for production

`npm run build`

### Load the application(s) in your browser

Assuming you've linked your config file to config.local.json:

<http://localhost:9000>

Otherwise, see values from your config file and/or the output from `npm run start`