/*eslint-disable */
import { html, PolymerElement } from "@polymer/polymer/polymer-element.js";

import template from "./contacts-summary.html";
import css from "./contacts-summary.css";
import * as d3 from "d3";

/**
 * @polymer
 * @extends HTMLElement
 */
export class GrmContactsSummary extends PolymerElement {
  static get properties() {
    return {};
  }

  static get template() {
    return html([
      `
        <style include="astro-css">
        ${css}
        </style> 
        ${template}`
    ]);
  }

  constructor() {
    super();
  }

  connectedCallback() {
    super.connectedCallback();
    this._buildBarChart();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
  }

  ready() {
    super.ready();
  }

  _buildBarChart() {
    const data = [{
        time: "0800",
        delayed: 5,
        progress: 10,
        planned: 5,
        completed: 6
      },
      {
        time: "0900",
        delayed: 6,
        progress: 9,
        planned: 4,
        completed: 6
      },
      {
        time: "0100",
        delayed: 5,
        progress: 7,
        planned: 8,
        completed: 7
      },
      {
        time: "0110",
        delayed: 5,
        progress: 8,
        planned: 7,
        completed: 11
      },
      {
        time: "1200",
        delayed: 7,
        progress: 8,
        planned: 6,
        completed: 4
      },
      {
        time: "1300",
        delayed: 7,
        progress: 9,
        planned: 8,
        completed: 8
      },
      {
        time: "1400",
        delayed: 9,
        progress: 10,
        planned: 10,
        completed: 9
      },
      {
        time: "1500",
        delayed: 7,
        progress: 9,
        planned: 6,
        completed: 5
      },
      {
        time: "1600",
        delayed: 6,
        progress: 10,
        planned: 4,
        completed: 5
      },
      {
        time: "1700",
        delayed: 8,
        progress: 7,
        planned: 8,
        completed: 8
      },
      {
        time: "1800",
        delayed: 8,
        progress: 9,
        planned: 9,
        completed: 6
      },
      {
        time: "1900",
        delayed: 5,
        progress: 6,
        planned: 11,
        completed: 11
      }
    ];

    // The data for each rect (bar) - it matches the data above
    const dataSet = [5, 6, 5, 5, 7, 7, 9, 7, 6, 8, 8, 5, 10, 9, 7, 8, 8, 9, 10, 9, 10, 7, 9, 6, 5, 4, 8, 7, 6, 8, 10, 6, 4, 8, 9, 11, 6, 6, 7, 11, 4, 8, 9, 5, 5, 8, 6, 11];

    // Keep the values of the Y element so that we can properly place the text within each rect.
    var yStore = [];
    var yStoreCount = 0;

    var series = d3.stack()
      .keys(["delayed", "progress", "planned", "completed"])
      .offset(d3.stackOffsetDiverging)
      (data);

    // set the dimensions and margins of the graph
    var margin = {
        top: 20,
        right: 20,
        bottom: 30,
        left: 40
      },
      width = 1600 - margin.left - margin.right,
      height = 450 - margin.top - margin.bottom;


    var xScale = d3.scaleBand()
      .domain(d3.range(data.length))
      .rangeRound([0, width])
      .paddingInner(0.05);

    var yScale = d3.scaleLinear()
      .domain([0, d3.max(data)])
      .range([0, height]);


    var svg = d3.select(this.shadowRoot.querySelector("#chart")).append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");


    var x = d3.scaleBand()
      .domain(data.map((d) => d.time))
      .rangeRound([margin.left, width - margin.right])
      .padding(0.5);

    var y = d3.scaleLinear()
      .domain([0, d3.max(series, (serie) => d3.max(serie, (d) => d[1]))])
      .rangeRound([height - margin.bottom, margin.top]);

    var z = d3.scaleOrdinal(['#6B290E', '#0C6364', '#3B3767', '#4D5C6B']);

    // gridlines in y axis function
    function make_y_gridlines() {
      return d3.axisLeft(y)
        .ticks(10)
    }

    //const makeYLines = () => d3.axisLeft()
    //  .scale(yScale)

    svg.append("g")
      .selectAll("g")
      .data(series)
      .enter().append("g")
      .attr("fill", function (d) {
        return z(d.key);
      })
      .selectAll("rect")
      .data(function (d) {
        return d;
      })
      .enter().append("rect")
      .attr("width", 60)
      .attr("x", function (d) {
        return x(d.data.time);
      })
      .attr("y", function (d) {
        yStore[yStoreCount] = y(d[1]);
        yStoreCount++;
        return y(d[1]);
      })
      .attr("height", function (d) {
        return y(d[0]) - y(d[1]);
      });

    svg.selectAll("text")
      .data(dataSet)
      .enter()
      .append("text")
      .text(function (d) {
        return d;
      })
      .attr("x", function (d, i) {
        // 0 - 11, 12 - 23, 24 - 35, 36 - 47
        if (i >= 0 && i <= 11) {
          return ++i * (1550 / 13);
        } else if (i > 11 && i <= 23) {
          let j = i - 11;
          return j++ * (1550 / 13);
        } else if (i > 23 && i <= 35) {
          let j = i - 23;
          return j++ * (1550 / 13);
        } else {
          let j = i - 35;
          return j++ * (1550 / 13);
        }
      })
      .attr("y", function (d, i) {
        if (i >= 0 && i <= 11) {
          return yStore[i] + 12;
        }
        else if (i > 11 && i <= 23) {
          return yStore[i] + 12;
        }
        else if (i > 23 && i <= 35) {
          return yStore[i] + 12;
        }
        else {
          return yStore[i] + 12;
        }
      })
      .attr("font-family", "OpenSan")
      .attr("font-size", "15px")
      .attr("fill", "white");


    svg.append("g")
      .attr("transform", "translate(0," + y(0) + ")")
      .call(d3.axisBottom(x))
      .attr("class", "axisWhite");

    svg.append("g")
      .attr("transform", "translate(" + margin.left + ",0)")
      .call(d3.axisLeft(y))
      .attr("class", "axisWhite");

    // Prep the tooltip bits, initial display is hidden
    var tooltip = svg.append("g")
      .attr("class", "tooltip")
      .style("display", "none");

    tooltip.append("rect")
      .attr("width", 30)
      .attr("height", 20)
      .attr("fill", "white")
      .style("opacity", 0.5);

    tooltip.append("text")
      .attr("x", 15)
      .attr("dy", "1.2em")
      .style("text-anchor", "middle")
      .attr("font-size", "12px")
      .attr("font-weight", "bold");

  }
}

customElements.define("grm-contacts-summary", GrmContactsSummary);
